<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderdiscountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderdiscounts', function($table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned();
			$table->decimal('discount_percent',5,2);
			$table->decimal('discount_value',15,2);
			$table->decimal('discounted_total',15,2);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orderdiscounts');
	}

}
