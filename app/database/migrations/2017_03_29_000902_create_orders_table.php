<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function($table)
		{
			$table->increments('id');
			$table->string('currency');
			$table->decimal('amount',15,2);
			$table->float('rate');
			$table->decimal('zar',5,2)->default(0.00);
			$table->decimal('surcharge_percent',5,2)->default(0.00);
			$table->decimal('surcharge_value',15,2)->default(0.00);
			$table->decimal('total',15,2);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
