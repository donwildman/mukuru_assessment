<?php


class SurchargeTableSeeder extends Seeder {

	public function run()
	{
		DB::table('surcharges')->delete();

		$currencyArray = [
			'USD' => 7.5,
			'GBP' => 5.0,
			'EUR' => 5.0,
			'KES' => 2.5
		];

		foreach($currencyArray as $key=>$value)
		{
			$currency = Currency::where('name', $key)->first();
			if(!empty($currency))
			{
				$surcharge = new Surcharge();
				$surcharge->currency_id = $currency->id;
				$surcharge->surcharge_percent = $value;
				$surcharge->save();
			}

		}

	}

}