<?php


class DiscountsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('discounts')->delete();

		$currencyArray = [
			'EUR' => 2.0
		];

		foreach($currencyArray as $key=>$value)
		{
			$currency = Currency::where('name', $key)->first();
			if(!empty($currency))
			{
				$discount = new Discount();
				$discount->currency_id = $currency->id;
				$discount->discount = $value;
				$discount->save();
			}

		}

	}

}