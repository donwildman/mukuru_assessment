<?php


use Carbon\Carbon;
class HomeController extends BaseController {

	protected $rates;

	public function __construct()
	{

	}

	public function showRates()
	{
		$currencies = Currency::get();

		if (!$currencies->count())
		{
			Session::flash("error","Looks like you forgot to populate the rates cache! Please run the command 'php artisan rates:update' from the terminal. ");
			return View::make('error');
		} else {

			$obj = [];
			$obj['base'] = 'USD';
			$obj['rates'] = [];

			foreach($currencies as $currency)
			{
				$obj['rates'][$currency->name] = $currency->val;
			}

			$data['obj'] = json_encode($obj);
			return View::make('buy', $data);
		}


	}

	public function postOrder()
	{
		$input = Input::except(['_token']);
		$rules = [
			'amount' => 'required|numeric',
			'zar' => 'required|numeric',
			'currency' => 'required',
		];

		$validator = Validator::make($input, $rules);

		if ($validator->fails())
		{
			Session::flash('error', 'There were errors in your input, please check your submission and try again.');
			return Redirect::to('buy')->withErrors($validator)->withInput();
		}

		$currency = Currency::with('surcharge','discount')->where('name', $input['currency'])->first();
		if(!empty($currency))
		{
			$input['rate'] = $currency->val;
			$input['surcharge_percent'] = 0;
			$input['surcharge_value'] = 0;

			if(!empty($currency->surcharge))
			{
				$surcharge_percent = $currency->surcharge->surcharge_percent;
				$input['surcharge_percent'] = $surcharge_percent;
				if($surcharge_percent > 0)
				{
					$input['surcharge_value'] = ($input['zar'] / 100) * $surcharge_percent;
				}
			}



			$input['total'] = $input['zar'] + $input['surcharge_value'];
		}

		if($order = Order::create($input))
		{

			$input['date'] = Carbon::now()->toDayDateTimeString();
			if(!empty($currency->discount))
			{
				$orderDiscount = new Orderdiscount();
				$orderDiscount->order_id = $order->id;

				$input['discount_percent'] = $orderDiscount->discount_percent = $currency->discount->discount;
				$input['discount_value'] = $orderDiscount->discount_value = ($input['total'] / 100) * $input['discount_percent'];
				$input['discounted_total'] = $orderDiscount->discounted_total = $input['total'] - $input['discount_value'];

				$orderDiscount->save();

			}
		}

		if($input['currency'] == 'GBP')
		{
			Log::info(Config::get('app.orderEmail'));
			Mail::queue('emails.order', $input, function ($message) {
				$message->to(Config::get('app.orderEmail'))->subject('Currency purchase Order');
			});
		}

		return View::make('summary', ['order'=>$input]);


	}

}
