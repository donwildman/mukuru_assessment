<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp\Client;


class UpdateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rates:update';
	private $client;
	private $ratesUrl;
	private $appID;

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	public function __construct()
	{
		parent::__construct();
		$this->ratesUrl = \Config::get( 'app.ratesUrl' );
		$this->appID = \Config::get( 'app.appID' );
		$this->client   = new Client(
			[
				'base_uri' => $this->ratesUrl,
				'timeout'         => 30,
				'connect_timeout' => 5
			]
		);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

			try {
				$request = $this->client->request( 'GET', '/api/latest.json',
					[
						'query'           => [ 'app_id' => $this->appID ]
					]
				);

				$response = $request->getBody();
				$res = json_decode($response, true);

				if(!isset($res['error']))
				{

					$rates = $res['rates'];
					foreach($rates as $key=>$value)
					{
						$currency = \Currency::firstOrCreate(array('name' => $key));
						$currency->val = $value;
						$currency->save();
					}
					$this->info( 'Rates cached!' );


				} else {
					$this->error( $res['description'] );

				}



			} catch ( BadResponseException $exception ) {
				$this->error( $exception->getMessage() );

			}

	}



}
