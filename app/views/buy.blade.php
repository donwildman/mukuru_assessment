@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-12">

            {{ Form::open(['url' => '/buy', 'role'=>'form', 'id'=>'ratesForm']) }}


            <div class="row">
                <div class="col-sm-5">
                    <p>Please select a currency from the list, then enter the amount you would like to purchase</p>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group  {{ ($errors->has('currency') ? 'has-error' : '') }}">
                                <label for="currency" class="sr-only">Currency</label>
                                {{ Form::select('currency', array('USD' => 'USD', 'GBP' => 'GBP', 'EUR' => 'EUR', 'KES' => 'KES'), Input::old('currency'), ['class'=>'form-control','id'=>'currency']) }}
                                <span class="help-block">{{ ($errors->has('currency') ? $errors->first('currency') : '') }}</span>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group  {{ ($errors->has('amount') ? 'has-error' : '') }}">
                                <label for="amount" class="sr-only">Amount</label>
                                <input type="text" name="amount" class="form-control" id="amount" placeholder="Amount" >
                                <span class="help-block">{{ ($errors->has('amount') ? $errors->first('amount') : '') }}</span>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="col-sm-1 text-center">
                    or
                </div>
                <div class="col-sm-5">
                    <p>Once you have chosen your currency, enter the value (in ZAR) that you would like to buy.</p>

                    <div class="row">
                        <div class="col-sm-2 text-right">
                            <span class="zar-label">ZAR</span>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group  {{ ($errors->has('zar') ? 'has-error' : '') }}">
                                <label for="zar" class="sr-only">Rand Value</label>
                                <input type="text" class="form-control" placeholder="Rand Value" name="zar" id="zar">
                                <span class="help-block">{{ ($errors->has('zar') ? $errors->first('zar') : '') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
                <button type="button" id="submitBtn" class=" btn btn-primary btn-sm m-t-20">Purchase</button>




            {{ Form::close() }}
        </div>
    </div>
    <script src="{{ asset('js/money.min.js') }}"></script>
    <script src="{{ asset('js/accounting.min.js') }}"></script>
    <script type="text/javascript">



        $(document).ready(function(){
            var _rates = {{ $obj }};
            fx.rates = _rates.rates;
            fx.base = _rates.base;

            $('#amount').keyup(function(){
                var _cur = $('#currency').val();
                var _amt = $(this).val();
                var _result = fx.convert(_amt, {from: _cur, to: "ZAR"});
                $('#zar').val(accounting.formatMoney(_result,'',2,'','.'));
            });

            $('#zar').keyup(function(){
                var _cur = $('#currency').val();
                var _amt = $(this).val();
                var _result = fx.convert(_amt, {from: "ZAR", to: _cur});
                $('#amount').val(accounting.formatMoney(_result,'',2,'','.'));
            });

            $('#currency').on('change', function(){
                var _cur = $(this).val();
                var _amt = $('#amount').val();
                var _result = fx.convert(_amt, {from: _cur, to: "ZAR"});
                $('#zar').val(accounting.formatMoney(_result,'',2,'','.'));
            });

            $('#submitBtn').on('click', function(){
                var _amount = $('#amount').val();
                var _zar = $('#zar').val();

                if($.isNumeric(_amount) && $.isNumeric(_zar))
                {
                    $('#ratesForm').submit();
                } else {
                    alert('Please enter a value!');
                }
            })

        });



    </script>


@stop
