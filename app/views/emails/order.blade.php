<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Order Confirmation</h2>

<div>
    An order was placed on the website with the following details:
    <p>
        Date: {{ $date }}<br/>
        Currency: {{ $currency }}<br/>
        Amount: {{ number_format($amount,2) }}<br/>
        Rate: {{ $rate }}<br/>
        Surcharge %: {{ $surcharge_percent }}<br/>
        Surcharge: {{ number_format($surcharge_value,2) }}<br/>
        Total: {{ number_format($total,2) }}<br/>
        -------------------------<br/><br/>
        @if(isset($discount_percent))
            This order qualified for a discount.<br/><br/>
            Discount %: {{ $discount_percent }}<br/>
            Discount: {{ number_format($discount_value,2) }}<br/>
            New Total: {{ number_format($discounted_total,2) }}
        @endif
    </p>
</div>
</body>
</html>
