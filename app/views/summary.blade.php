@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h3>Thank You!</h3>
            <p>An order was placed on the website with the following details:</p>
            <p>
                Date: {{ $order['date'] }}<br/>
                Currency: {{ $order['currency'] }}<br/>
                Amount: {{ number_format($order['amount'],2) }}<br/>
                Rate: {{ $order['rate'] }}<br/>
                Surcharge %: {{ $order['surcharge_percent'] }}<br/>
                Surcharge: {{ number_format($order['surcharge_value'],2) }}<br/>
                Total: {{ number_format($order['total'],2) }}<br/>
                -------------------------<br/><br/>
                @if(isset($order['discount_percent']))
                    This order qualified for a discount.<br/><br/>
                    Discount %: {{ $order['discount_percent'] }}<br/>
                    Discount: {{ number_format($order['discount_value'],2) }}<br/>
                    New Total: {{ number_format($order['discounted_total'],2) }}
                @endif
            </p>

        </div>
    </div>

    <pre>{{ print_r($order) }}</pre>
@stop
