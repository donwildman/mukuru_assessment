<?php


class Orderdiscount extends Eloquent  {

	public $timestamps = false;

	public function order()
	{
		return $this->belongsTo('Order');
	}
}
