<?php


class Order extends Eloquent  {

	protected $fillable = ['currency','amount','rate','zar','surcharge_percent','surcharge_value','total'];

	public function discount()
	{
		return $this->hasOne('Orderdiscount');
	}


}
