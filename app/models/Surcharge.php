<?php


class Surcharge extends Eloquent  {

	public $timestamps = false;

	public function currency()
	{
		return $this->belongsTo('Currency');
	}

}
