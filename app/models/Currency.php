<?php


class Currency extends Eloquent  {

	protected $table = 'currencies';

	protected $fillable = array('name', 'val');

	public $timestamps = false;

	public function surcharge()
	{
		return $this->hasOne('Surcharge');
	}
	public function discount()
	{
		return $this->HasOne('Discount');
	}
}
