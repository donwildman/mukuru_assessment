Setup:

Clone the repo to your local virtual host.

Do a composer update to fetch vendor files.

Update the app.php config file with the email address of the recipient for any orders emails.

Create your database and populate the details in the database.php config file.

Migrate the db with 'php artisan migrate'.

Update the Currency cache with 'php artisan rates:update'.

Seed the db with 'php artisan db:seed'.

Point your browser to the local host.


Notes:

I have set the mail provider to 'log' in case your local environment doesn't accomodate outgoing mails.